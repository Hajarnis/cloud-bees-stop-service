package com.hsbc.google.api.stopinstanceservice;

public class TestStopComputeEngine {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Starting to delete instance");
		
		StopComputeEngine newComputeEngine = new StopComputeEngine(
				"EEP Hackathon",
				"lively-epsilon-170708",
				"asia-southeast1-a",
				"my-sample-instance"
				);
		
		String op = newComputeEngine.computeInstance();
		
		System.out.println("Result :: " + op);
		
		System.out.println("Completed.");
	}

}
