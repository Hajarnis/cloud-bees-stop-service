package com.hsbc.google.api.stopinstanceservice;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@EnableDiscoveryClient
@SpringBootApplication
public class StopInstanceServiceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(StopInstanceServiceApplication.class, args);
	}
}

@RefreshScope
@RestController
class DeleteInstanceRestController{
	
	

	@RequestMapping(value="/stopInstance", method=RequestMethod.POST)
	public String stopInstance(@RequestBody StopComputeEngine stopComputeEngine){
		
		StopComputeEngine newComputeEngine = new StopComputeEngine(
				stopComputeEngine.getApplicationName(),
				stopComputeEngine.getProjectId(),
				stopComputeEngine.getZoneName(), 
				stopComputeEngine.getSampleInstanceName());
		
		return newComputeEngine.computeInstance();
	}
}

