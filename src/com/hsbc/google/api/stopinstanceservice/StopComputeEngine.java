package com.hsbc.google.api.stopinstanceservice;

/*
 * Copyright 2015 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.ComputeScopes;
import com.google.api.services.compute.model.AccessConfig;
import com.google.api.services.compute.model.AttachedDisk;
import com.google.api.services.compute.model.AttachedDiskInitializeParams;
import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.InstanceList;
import com.google.api.services.compute.model.Metadata;
import com.google.api.services.compute.model.NetworkInterface;
import com.google.api.services.compute.model.Operation;
import com.google.api.services.compute.model.ServiceAccount;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



/**
 * Command-line sample to demo listing Google Compute Engine instances using Java and the Google
 * Compute Engine API.
 *
 * @author Jonathan Simon
 */

public class StopComputeEngine {

  public StopComputeEngine() {
		super();
		// TODO Auto-generated constructor stub
	}

public StopComputeEngine(String applicationName, String project_Id, String zone_Name, String sample_Instance_Name) {
	  
	  this.applicationName = applicationName;
	  this.projectId = project_Id;
	  this.zoneName = zone_Name;
	  this.sampleInstanceName = sample_Instance_Name;	  	
	}

public String getApplicationName() {
	return applicationName;
}

public void setApplicationName(String applicationName) {
	this.applicationName = applicationName;
}

public String getProjectId() {
	return projectId;
}

public void setProjectId(String projectId) {
	this.projectId = projectId;
}

public String getZoneName() {
	return zoneName;
}

public void setZoneName(String zoneName) {
	this.zoneName = zoneName;
}

public String getSampleInstanceName() {
	return sampleInstanceName;
}

public void setSampleInstanceName(String sampleInstanceName) {
	this.sampleInstanceName = sampleInstanceName;
}


/**
   * Be sure to specify the name of your application. If the application name is {@code null} or
   * blank, the application will log a warning. Suggested format is "MyCompany-ProductName/1.0".
   */
  private String applicationName;// = "EEP Hackathon";

  /** Set PROJECT_ID to your Project ID from the Overview pane in the Developers console. */
  private String projectId;// = "surveillance-it";

  /** Set Compute Engine zone. */
  private String zoneName;// = "us-east1-d";

  /** Set the name of the sample VM instance to be created. */
  private String sampleInstanceName;// = "surv-003";

  /** Set the path of the OS image for the sample VM instance to be created.  */
  private String sourceImagePrefix;// = "https://www.googleapis.com/compute/v1/projects/";
  private String sourceImagePath;// =
      //"debian-cloud/global/images/debian-7-wheezy-v20150710";

  /** Set the Network configuration values of the sample VM instance to be created. */
  private static  String NETWORK_INTERFACE_CONFIG = "ONE_TO_ONE_NAT";
  private static  String NETWORK_ACCESS_CONFIG = "External NAT";

  /** Set the time out limit for operation calls to the Compute Engine API. */
  private static final long OPERATION_TIMEOUT_MILLIS = 60 * 1000;

  /** Global instance of the HTTP transport. */
  private static HttpTransport httpTransport;

  /** Global instance of the JSON factory. */
  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

  
  
  public String computeInstance(){
	  try {
	      httpTransport = GoogleNetHttpTransport.newTrustedTransport();

	      // Authenticate using Google Application Default Credentials.
	      GoogleCredential credential = GoogleCredential.getApplicationDefault();
	      if (credential.createScopedRequired()) {
	        List<String> scopes = new ArrayList<String>();
	        // Set Google Cloud Storage scope to Full Control.
	        scopes.add(ComputeScopes.DEVSTORAGE_FULL_CONTROL);
	        // Set Google Compute Engine scope to Read-write.
	        scopes.add(ComputeScopes.COMPUTE);
	        credential = credential.createScoped(scopes);
	      }

	      // Create Compute Engine object for listing instances.
	        
	      Compute compute =
	          new Compute.Builder(httpTransport, JSON_FACTORY, credential)
	              .setApplicationName(getApplicationName())
	              .build();

	      // List out instances, looking for the one created by this sample app.
	      boolean foundOurInstance = printInstances(compute);

	      Operation op;
	      if (foundOurInstance) {
	    	System.out.println("Stopping instance :: " + getSampleInstanceName());
	        op = stopInstance(compute, getSampleInstanceName());
	      } else {
	    	System.out.println("Instance " + getSampleInstanceName() + " does not exist/is inactive. Cannot be deleted");
	        op = null;
	      }

	      // Call Compute Engine API operation and poll for operation completion status
	      System.out.println("Waiting for operation completion...");
	      Operation.Error error = blockUntilComplete(compute, op, OPERATION_TIMEOUT_MILLIS);
	      if (error == null) {
	        return "Success!";
	      } else {
	        return error.toPrettyString();
	      }
	    } catch (IOException e) {
	      System.err.println(e.getMessage());
	    } catch (Throwable t) {
	      t.printStackTrace();
	    }
	  return null;
  }

  public static void main(String[] args) {  
  }

  // [START list_instances]
  /**
   * Print available machine instances.
   *
   * @param compute The main API access point
   * @return {@code true} if the instance created by this sample app is in the list
   */
  public boolean printInstances(Compute compute) throws IOException {
    System.out.println("================== Listing Compute Engine Instances ==================******");
    Compute.Instances.List instances = compute.instances().list(getProjectId(), getZoneName());
    
    InstanceList list = instances.execute();
    boolean found = false;
    if (list.getItems() == null) {
      System.out.println("No instances found. Sign in to the Google Developers Console and create "
          + "an instance at: https://console.developers.google.com/");
    } else {
      for (Instance instance : list.getItems()) {
        System.out.println(instance.toPrettyString());
        if (instance.getName().equals(getSampleInstanceName())) {
          found = true;
        }
      }
    }
    return found;
  }
  // [END list_instances]


  private Operation stopInstance(Compute compute, String instanceName) throws Exception {
    System.out.println(
        "================== Stopping Instance " + instanceName + " ==================");
    Compute.Instances.Stop stop =
        compute.instances().stop(getProjectId(), getZoneName(), instanceName);
    return stop.execute();
  }

  // [START wait_until_complete]
  /**
   * Wait until {@code operation} is completed.
   * @param compute the {@code Compute} object
   * @param operation the operation returned by the original request
   * @param timeout the timeout, in millis
   * @return the error, if any, else {@code null} if there was no error
   * @throws InterruptedException if we timed out waiting for the operation to complete
   * @throws IOException if we had trouble connecting
   */
  public Operation.Error blockUntilComplete(
      Compute compute, Operation operation, long timeout) throws Exception {
    long start = System.currentTimeMillis();
    final long pollInterval = 5 * 1000;
    String zone = operation.getZone();  // null for global/regional operations
    if (zone != null) {
      String[] bits = zone.split("/");
      zone = bits[bits.length - 1];
    }
    String status = operation.getStatus();
    String opId = operation.getName();
    while (operation != null && !status.equals("DONE")) {
      Thread.sleep(pollInterval);
      long elapsed = System.currentTimeMillis() - start;
      if (elapsed >= timeout) {
        throw new InterruptedException("Timed out waiting for operation to complete");
      }
      System.out.println("waiting...");
      if (zone != null) {
        Compute.ZoneOperations.Get get = compute.zoneOperations().get(getProjectId(), zone, opId);
        operation = get.execute();
      } else {
        Compute.GlobalOperations.Get get = compute.globalOperations().get(getProjectId(), opId);
        operation = get.execute();
      }
      if (operation != null) {
        status = operation.getStatus();
      }
    }
    return operation == null ? null : operation.getError();
  }
  // [END wait_until_complete]
}